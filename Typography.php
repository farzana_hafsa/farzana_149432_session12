<h1> This is heading tag h1 </h1>
<h2> This is heading tag h2</h2>
<h3> This is heading tag h3 </h3>
<p>This text is large but<small>This text is small</small></p>
<p>This is another paragraph.</p>
<del>This line of text is meant to be treated as deleted text.</del></br>
<mark>highlight</mark><br>

<u>this text is underlined.</u>

<b>This text is bold</b><br>
<i>This text is italic.</i><br>
<blockquote>This is a long quotation.
    This is a long quotation.This is a long quotation.
    This is a long quotation.</blockquote><br>
The <abbr title="World Health Organization">WHO</abbr> was founded in 1948.<br>

Do the work<acronym title="As Soon As Possible">
    ASAP</acronym><br>

