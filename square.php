<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Square</title>
    <style>

        .box {
            background-color: #008000;
            height: 100px;
            width: 100px;
            display: inline-block;
            margin-left:20px;
            margin-bottom: 20px;
        }


        .box-text {
            position: relative;
            top:50%;
            left:50%;
        }


        .container {
            height: 600px;
            width: 600px;
            margin: 0 auto;
        }

        .header {
            text-align:center;
            font-weight: bold;
            font-family: Georgia;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="header">
        <p>Farzana Begum B33 149432 </p>
    </div>
    <div class="box"><b class="box-text">1</b></div>
    <div class="box"><b class="box-text">2</b></div>
    <div class="box"><b class="box-text">3</b></div>
    <div class="box"><b class="box-text">4</b></div>
    <div class="box"><b class="box-text">5</b></div>
    <div class="box"><b class="box-text">6</b></div>
    <div class="box"><b class="box-text">7</b></div>
    <div class="box"><b class="box-text">8</b></div>
    <div class="box"><b class="box-text">9</b></div>
    <div class="box"><b class="box-text">10</b></div>
    <div class="box"><b class="box-text">11</b></div>
    <div class="box"><b class="box-text">12</b></div>
    <div class="box"><b class="box-text">13</b></div>
    <div class="box"><b class="box-text">14</b></div>
    <div class="box"><b class="box-text">15</b></div>
    <div class="box"><b class="box-text">16</b></div>
</div>
</body>
</html>